/*
 * Praedic: Plasma RAE Dictionary v0.5.1
 *
 * Copyright 2010-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, see <http://www.gnu.org/licenses/>
 *
 */

print("Praedic v0.5.1-dev")
print("Plasma/JS API version " + plasmoid.apiVersion)
plasmoid.AspectRatioMode = IgnoreAspectRatio


//htmlPre = "<body bgcolor=#AAAAAA><font color=#1010CC><b>"
//htmlPost = "</b></font></body>"

htmlPre = "<body><style>{ background-color: #FFFFFF, color: #000000 }</style><b>"
htmlPost = "</b></body>"

var query = String("")



function LoadSearch()
{
    /* http://lema.rae.es/drae/srv/search?type=3&val=test&val_aux=&origen=RAE */

    if (searchBox.text == "")
    {
        web.html = htmlPre + "Introduce una palabra para buscar" + htmlPost
        return
    }

    web.html = htmlPre + "Consultando..." + htmlPost
    plasmoid.busy = true
    // query = "http://buscon.rae.es/draeI/SrvltGUIBusUsual?TIPO_HTML=2&LEMA="+searchBox.text+"&FORMATO=ampliado"
    query = "http://lema.rae.es/drae/srv/search?val=" + escape(searchBox.text) + "&type=3"
    print(query)
    web.url = Url(query)
}



function SetNewZoom()
{
    web.zoomFactor = zoomSlider.value / 10.0
    // zoomSlider.visible = false
    // zoomSlider.enabled = false
}



function WebURLChanged()
{
    plasmoid.busy = true
}



function WebLoadFinished()
{
    plasmoid.busy = false

    print("Host ---> " + web.url.host)
    if ( web.url.host == "lema.rae.es" || web.url.toString == "about:blank" )
    {
        print("OK, still at lema.RAE.es or about:blank")
        //web.html = htmlPre + web.html + htmlPost
    }
    else
    {
        print("We're out of lema.RAE.es!!")
	web.url = Url(query)
    }
    // ???  web.scrollPosition = QPoint(30,20)
}



mainLayout = new LinearLayout(plasmoid)
mainLayout.orientation = QtVertical
mainLayout.setAlignment(QtAlignTop)

upperLayout = new LinearLayout(mainLayout)
upperLayout.orientation = QtHorizontal
upperLayout.setAlignment(QtAlignTop)

lowerLayout = new LinearLayout(mainLayout)
lowerLayout.orientation = QtHorizontal

mainLayout.addItem(upperLayout)
mainLayout.addItem(lowerLayout)


findLabel = new Label()
findLabel.text = "Definir palabra:  "
upperLayout.addItem(findLabel)

searchBox = new LineEdit()
searchBox.clearButtonShown = true
searchBox.returnPressed.connect(LoadSearch)
upperLayout.addItem(searchBox)



button = new PushButton()
button.text = "&Buscar en RAE"
button.clicked.connect(LoadSearch)
upperLayout.addItem(button)


web = new WebView()
web.html = htmlPre + "Escribe la palabra que deseas consultar en el cuadro de texto de la parte superior." + htmlPost
web.zoomFactor = 1.0

//web.fontSize = 20

// WARNING: dragToScroll removes scrollbars
//web.dragToScroll = true

web.urlChanged.connect(WebURLChanged)
web.loadFinished.connect(WebLoadFinished)
lowerLayout.addItem(web)


zoomSlider = new Slider()
zoomSlider.setMaximum(15)
zoomSlider.setMinimum(5)
zoomSlider.value = web.zoomFactor * 10.0
lowerLayout.addItem(zoomSlider)
zoomSlider.valueChanged.connect(SetNewZoom)


